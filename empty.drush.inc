<?php
define('DELETE_ALL_MAX_EXECUTION', 600);

/**
 * Implementation of hook_drush_command().
 */
function empty_drush_command() {
  $items = array();
  $items['empty'] = array(
    'description' => 'drush command to delete content in your site',
    'aliases' => array('emt'),
    'examples' => array(
      'Delete all nodes of particular author(name)' => 'drush empty node --author=admin',
      'Delete all nodes of particular uid(uid of author)' => 'drush empty node --uid=1',
    ),
    'arguments' => array(
      'entity' => 'Entity - node, user, voc (Vocabulary), menu',
    ),
    'options' => array(
      'bundle' => 'The machine name of bundle, Ex: node (article, page), voc (tags) ',
      'role' => 'The machine name of bundle, Ex: node (article, page), voc (tags) ',
      'name' => 'username or vocabulary name',
      'nid' => 'node nid',
      'uid' => 'user uid of user object or author of node',
      'tid' => 'taxonomy tid',
      'author' => 'node author'
    ),
  );

  return $items;
}

/*
 *
 */
function empty_get_options($level = NULL) {
  switch ($level) {
    case NULL:
      return array(
        'all' => 'All',
        'nodes' => 'Nodes',
        'users' => 'Users',
        'voc' => 'All Vocabularies',
      );
      break;
    case 'node':
      return array(
        'all' => 'All',
        'published' => 'Published Nodes',
        'unpublished' => 'Unpublished Nodes'
      );
      break;
    case 'user':
      return array(
        'all' => 'All',
        'active' => 'Active Users',
        'block' => 'Blocked Users'
      );
      break;
    case 'voc':
      return array(
        'all' => 'All',
        // todo : might be list out vocabularies list
      );
      break;
  }
}

/*
 *
 */
function drush_empty($entity = NULL) {
  switch ($entity) {
    case NULL:
      empty_all_start();
      break;
    case 'node':
      empty_node_start();
      break;
    default:
      drush_print(dt("Invalid entity argument \n\r available entity arguments are node, user, voc, menu"));
  }
}

function empty_all_start() {
  $options = empty_get_options();
  $choice = drush_choice($options, dt('What entities you want to clean up ?'));
  if ($choice) {
    //drush_print(dt('Hello @choice!', array('@choice' => $options[$choice])));
    // todo code here
  }
}

function empty_node_conditions_build($conditions) {
  $required_options = array('nid', 'type', 'uid');
  $user_input_options = array_keys($conditions);

  $nids = db_select('node', 'n');

  $nids->fields('n', array('nid'));

  foreach ($required_options as $each) {
    if (in_array($each, $user_input_options)) {
      if (!empty($conditions[$each])) {
        $nids->condition("$each", $conditions[$each]);
      }
    }
  }
  $node_options_display_to_user = empty_get_options('node');
  $status = drush_choice($node_options_display_to_user, dt('Take a option'));

  switch ($status) {
    case 'published':
      $nids->condition("status", 1);
      break;
    case 'unpublished':
      $nids->condition("status", 0);
      break;
    case 'all':
      break;
  }

  $nids = $nids->execute()->fetchCol();

  return $nids;
}

function empty_node_delete($nids) {
  // todo delete query with status message
  // drush_print(dt(implode(', ',$nids)));
  //ini_set('max_execution_time', DELETE_ALL_MAX_EXECUTION);

  if ($nids) {
    $count = count($nids);
//    if (drush_confirm("Are you sure you want to delete $count nodes ?")) {
      //node_delete_multiple(array($nids));
      foreach ($nids as $nid) {
        set_time_limit(30);
        node_delete($nid);
        drush_print(dt("Deleted node with nid - $nid"));
      }
      watchdog('empty', 'Empty module deleted nodes with nid -  @nodes', array('@nodes' => implode(', ', $nids)));
//    }
//    else {
//      drush_user_abort();
//    }
  }
}

/*
 * Function executes node deletion operations based on particular conditions
 */
function empty_node_start() {

  $conditions = array(
    'nid' => drush_get_option_list('nid', NULL),
    'type' => drush_get_option_list('bundle', NULL),
    'uid' => drush_get_option_list('uid', NULL),
  );

  $nids = empty_node_conditions_build($conditions);
  empty_node_delete($nids);

}

/*
 * Function to delete all nodes
 *
 * @param $bundle
 *   name of bundle, Ex: article, page
 * @param $uid
 *   user uid / author uid
 */
function _emtpy_node_all() {

  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->execute()
    ->fetchCol();

  //node_delete_multiple($nids);

  if ($nids) {
    drush_print(dt("all nodes deleted"));
  }
  else {
    drush_print(dt('No nodes deleted!'));
  }
}

/*
 * Function to delete nodes of user
 *
 * @param $bundle
 *   name of bundle, Ex: article, page
 * @param $uid
 *   user uid / author uid
 */
function _emtpy_node_nid($nid) {

  $options = array(
    'world' => 'World',
    'univers' => 'Univers',
    'planet' => 'Planet',
  );

  $choice = drush_choice($options, dt('Who do you want to say hello to?'));

  if ($choice) {
    drush_print(dt("Hello $nid"));
  }

  $nids_input = preg_split('/[\ \n\,]+/', $nid);

  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('nid', $nids_input)
    ->execute()
    ->fetchCol();

  //node_delete_multiple($nids);

  if ($nids) {
    $deleted_nids = implode(', ', $nids);
    drush_print(dt($deleted_nids . " nid nodes deleted"));
  }
  else {
    drush_print(dt('No nodes deleted!, Check bundle name/s'));
  }
}

/*
 * Function to delete nodes of user
 *
 * @param $bundle
 *   name of bundle, Ex: article, page
 * @param $uid
 *   user uid / author uid
 */
function _emtpy_node_uid($uid) {
  $nids = FALSE;
  if (!is_array($uid)) {
    $uids = preg_split('/[\ \n\,]+/', $uid);
  }
  elseif (!empty($uid)) {
    $uids = $uid;
  }
  else {
    drush_print(dt('No nodes deleted!, Check author/uid'));
  }

  if ($uid) {
    $nids = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('uid', $uids)
      ->execute()
      ->fetchCol();
  }
  //node_delete_multiple($nids);

  if ($nids) {
    $deleted_nids = implode(', ', $nids);
    drush_print(dt($deleted_nids . " nid nodes deleted"));
  }
}

/*
 * Function to delete nodes of particular type and user
 *
 * @param $bundle
 *   name of bundle, Ex: article, page
 * @param $uid
 *   user uid / author uid
 */
function _emtpy_node_bundle_uid($bundle, $uid) {

  $bundles = preg_split('/[\ \n\,]+/', $bundle);
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', $bundles)
    ->condition('uid', $uid)
    ->execute()
    ->fetchCol();

  //node_delete_multiple($nids);

  if ($nids) {
    $deleted_nids = implode(', ', $nids);
    drush_print(dt($deleted_nids . " nid nodes deleted"));
  }
  else {
    drush_print(dt('No nodes deleted!, Check bundle name/s'));
  }
}

/*
 * Function to delete nodes of particular type
 *
 * @param $bundle
 *   name of bundle, Ex: article, page
 */
function _emtpy_node_bundle($bundle) {

  $bundles = preg_split('/[\ \n\,]+/', $bundle);
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', $bundles)
    ->execute()
    ->fetchCol();

  //node_delete_multiple($nids);

  if ($nids) {
    $deleted_nids = implode(', ', $nids);
    drush_print(dt($deleted_nids . " nid nodes deleted"));
  }
  else {
    drush_print(dt('No nodes deleted!, Check bundle name/s'));
  }

}

function _empty_user_execute() {
  drush_print(dt('in user'));
}

function _empty_voc_execute() {
  drush_print(dt('in voc'));
}

function _empty_menu_execute() {
  drush_print(dt('in menu'));
}

function empty_node_count($type) {

  $type = drush_choice(array('Yes'), 'are youoooooo');
  drush_user_abort();

  // Get arguments passed in command, Ex: drush nc page blog
  $args = func_get_args();

  $repeat = drush_get_option('repeat', 1);

  drush_print($repeat . ' --- ' . $type);

  if ($args) {
    // Loop for types
    foreach ($args as $type) {
      // Query to get count of particular type
      $result = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('type', $type, '=')
        ->execute();
      $num_of_results = $result->rowCount();
      drush_print($type . "'s node count : " . $num_of_results);
    }

  }
  // If no type passed then return total count
  else {
    drush_print('No node types mentioned');

    $result = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->execute();
    $num_of_results = $result->rowCount();

    drush_print('Count of all nodes : ' . $num_of_results);
  }
}